package multiframer;

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;

class Main extends JPanel implements FrameOwner
{   
	// image name NB: if still using 'happy_fish' or similar this is not my IP so do not publish
	
	// main image
	final String backImageName = "src/images/happy_fish2.gif";
	private Image backImage;
	private int frameWidth  = 500;
	private int frameHeight = 300;
	
	// layer1 image 
	final String boatImageName = "src/images/boat2.gif";
	private Image boatImage;
	private int later1Width  = 75;
	private int layer1height = 75;
	
	private Image currentImage;
	private int displayCount = 0;

	
	// PrepRun is runner object
	private FrameRunner runner;
	
	//--- graphics canvas ----------------
	final JPanel canvas;


    public Main()
    {   
	    // deal with any essential contruction vars
	    runner = new FrameRunner( this, frameWidth, frameHeight );
	    
	    //----------------------------------------
	    // load images & initialise buffer
	    //----------------------------------------
	    
	    MediaTracker mediaTracker = new MediaTracker(this);
    	backImage = Toolkit.getDefaultToolkit().getImage(backImageName);
    	mediaTracker.addImage(backImage, 0);
    	boatImage = Toolkit.getDefaultToolkit().getImage(boatImageName);
    	mediaTracker.addImage(boatImage, 0);
		try {	mediaTracker.waitForID(0);
            } 	catch (InterruptedException e) {
                // carry on anyway
                System.out.println( "probblem loading images" );
            }

	    //----------------------------------------
	    // setup GUI
	    //----------------------------------------
	    
	    Main main = this;   									// *** sensible(?) HACK *** see notes why
		main.setLayout( new BorderLayout() );
		
		//--- graphics canvas ----------------
		canvas = new JPanel()
		{	public void paintComponent( Graphics g )
			{	super.paintComponent(g);
				/**/ Defs.debug( "MAIN -- paintComponent" );
				if( currentImage != null )							// because at the start no data is ready
				{	g.drawImage( currentImage, 0, 0, this );
				}
			}
		};
		
		main.add( canvas, BorderLayout.CENTER );
		
		//--- cntrlPanel ---------------------
		final JPanel cntrlPanel = new JPanel();
		cntrlPanel.setLayout( new FlowLayout() );
		main.add( cntrlPanel, BorderLayout.NORTH );
		
		JButton goBtn = new JButton("start");
		goBtn.addActionListener(new ActionListener()
		{	public void actionPerformed(ActionEvent evt)
			{	runner.start();
			}
		});
		cntrlPanel.add( goBtn );
		
		JButton pauseBtn = new JButton("stop");
		pauseBtn.addActionListener(new ActionListener()
		{	public void actionPerformed(ActionEvent evt)
			{	runner.stop();
			}
		});
		cntrlPanel.add( pauseBtn );
    }
	/**
	 * displayFrame is required by FrameOwners and is called when a new frame 
	 * is ready to display (ie: it has been prepared and the time between frame
	 * displays has elapsed)
	 * @param frame 
	 */
	public void displayFrame( BufferedImage frame )
	{	currentImage = frame;
		canvas.repaint();
	}
	/**
	 * prepareFrame is required by FrameOwners and is called when a new frame 
	 * is ready to be prepared, ie: there is space in the frame buffer array
	 * @param frame 
	 */
	public void prepareFrame( BufferedImage frame )
	{	// hard quit for safety
		if( displayCount > 200 )
		{	/**/ Defs.debug( "MAIN -- stopping therads" );
			runner.stop();
			if( JOptionPane.YES_OPTION ==
            		JOptionPane.showConfirmDialog( this,
            			"auto stop through timeout. Restart Y/N" ))
	            	{	displayCount=0;
						runner.start();
        			}
		}
		else
		{	int x = (displayCount*2) % frameWidth;
			Graphics g = frame.getGraphics();
			g.drawImage( backImage, 0, 0, this);
			g.drawImage( boatImage, x, 150, this);
			displayCount++;
		}
	}
}

public class MultiFramer
{
    public static void main(String []args)
    {	
        // give title for title bar of window
        JFrame frame = new JFrame("Animating frames #1");
        Main panel = new Main();
        frame.add(panel);
        
        // exit the program when window is close
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frame.setSize( 500, 350 ); // suggest width and height of frame in pixels
        frame.setVisible(true);
    }
}
