package multiframer;


public class Defs
{	// please discuss the use of this type of class with S
	// this approach fits a design pattern & overcomes some
	// messy problems but some people don't like the approach
	
	
	public static boolean debugOn = true;
	/** a brief time delay mechanism
		delay is in millisecs
	*/
	public static void snooze(int delay)
	{	if (delay == 0) return;		// catch no delay case
		try {
			Thread.sleep( delay );
		} catch ( InterruptedException e ) {}
	}
	/**************
	***  debug  ***
	***************/
	public static void debug( String s )
	{	if( debugOn ) System.out.println( s );
	}
	
	public static void debug( String name, Object val )
	{	String vstr = (val == null) ? "null" : getClassNval( val );
		debug( name + " = " + vstr );
	}
	
	public static void debug( String label, String name, Object val )
	{	debug( label + ":   " + name, val );
	}
	
	/***************
	***  debugN  ***
	***************/
	public static void debugN( String name, Object val )
	{	String vstr = (val == null) ? "null" : "non-null";
		debug( name + " = " + vstr );
	}
	
	public static void debugN( String label, String name, Object val )
	{	debugN( label + ":   " + name, val );
	}
	
	public void internAll( String[] str )
    {	for( String s : str )
    		s.intern();
    }

	private static String getClassNval( Object o )
	{	if( o == null )
			return "null";
		else
			return "<"+ o.getClass().toString() +"> "+ o.toString();
	}
}