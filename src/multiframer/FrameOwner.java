package multiframer;

import java.awt.image.BufferedImage;



public interface FrameOwner
{	/**
	 * display a prepared frame
	 * @param frame the frame to be displayed
	 */
	public void displayFrame( BufferedImage frame );
	/**
	 * prepare the next frame
	 * @param frame the frame to be prepared
	 */
	public void prepareFrame( BufferedImage frame );
}
