package multiframer;

import java.awt.image.BufferedImage;

/**
 * this implements a producer/consumer system for preparing/displaying images using 
 * an image buffer array to hold pre-prepared images
 * @author s
 */
public class FrameRunner
{	
	private FrameOwner parent;				// standard pattern, parent gets passed frames to prepare/display
	
	private PreparingThread prepThread;		// the thread to control preparation
	private DisplayingThread useThread;		// the thread to trigger use
	
	private int dataLength;					// no. data elements used
	private int elementInUse;				// the data element in use - don't mess with this one
	private int elementInPrep;				// the data element being prep'd
	private int prepNum;					// the number of times data use has been prep'd
	private int useDelay;					// usage delay
	private int prepDelay;					// delay for re-preping
	
	final static int defaultNoFrames = 4;
	
	private BufferedImage[] images;
	
	/**
	 * construct a FrameRunner with default frame buffer size
	 * @param parent        the implementing FrameOwner
	 * @param frameWidth	image width & height
	 * @param frameHeight 
	 */
	public FrameRunner( FrameOwner parent, int frameWidth, int frameHeight )
	{	this( parent, frameWidth, frameHeight, defaultNoFrames );
	}
	/**
	 * construct a FrameRunner specifying frame buffer size
	 * @param parent
	 * @param frameWidth
	 * @param frameHeight
	 * @param noFrames		number of frames used by the frame buffer
	 */
	public FrameRunner( FrameOwner parent, int frameWidth, int frameHeight, int noFrames )
	{	this.parent = parent;
		this.dataLength = noFrames;
		prepThread = new PreparingThread();
		useThread = new DisplayingThread();
		
		// default values
		setElementInUse( 0 );
		setElementInPrep( 0 );			// is pre-incremented
		setPrepNum( 0 );
		setUseDelay( 30 );				// defaults to about 30-40 repeats sec
		
		images = new BufferedImage[noFrames];
		// NB: TYPE_INT_RGB dosnt allow transparency
		for( int i=0; i<images.length; i++ )
    		images[i] = new BufferedImage(frameWidth, frameHeight, BufferedImage.TYPE_INT_RGB);
	}
	/**
	 * start preparing & displaying threads
	 */
	public void start()
	{	prepThread.start();
		useThread.start();
	}
	/**
	 * stop preparing & displaying threads
	 */
	public void stop()
	{	prepThread.stop();
		useThread.stop();
	}
	// accessors & mutators
	public void setUseDelay( int useDelay )
	{	this.useDelay = useDelay;
		setPrepDelay( useDelay * dataLength / 2 );		// only kick prep in 1/2 the time by default
	}
	public int getUseDelay()
	{	return useDelay;
	}
	private void setDataLength( int dataLength )
	{	this.dataLength = dataLength;
	}
	private int getDataLength()
	{	return dataLength;
	}
	private void setElementInUse( int elementInUse )
	{	this.elementInUse = elementInUse;
	}
	private int getElementInUse()
	{	return elementInUse;
	}
	private void setElementInPrep( int elementInPrep )
	{	this.elementInPrep = elementInPrep;
	}
	private int getElementInPrep()
	{	return elementInPrep;
	}
	private void setPrepNum( int prepNum )
	{	this.prepNum = prepNum;
	}
	private int getPrepNum()
	{	return prepNum;
	}
	private void setPrepDelay( int prepDelay )
	{	this.prepDelay = prepDelay;
	}
	private int getPrepDelay()
	{	return prepDelay;
	}
	
	
	// local classes PreparingThread & TriggeringThread
	
	private class DisplayingThread implements Runnable
	{
		private Thread thread;					// class owns its thread
		private boolean stopSignalled;			// as it says

		public void start()
		{	stopSignalled = false;
			if( thread == null || !thread.isAlive() )
			{	thread = new Thread(this);
				thread.start();
			}
		}
		public void stop()
		{	stopSignalled = true;
		}
		
		public void run()
		{	/**/ Defs.debug( "DisplayingThread starts" );
			while( !stopSignalled )
			{	parent.displayFrame( images[elementInUse] );
				elementInUse = (elementInUse + 1) % dataLength;
				Defs.snooze( useDelay );
			}
			/**/ Defs.debug( "DisplayingThread ends" );
		}
	}

	private class PreparingThread implements Runnable
	{	
		private boolean stopSignalled;			// someone (the parent) has requested the thread stops
		private Thread thread;					// this class's own thread
	
		public void start()
		{	stopSignalled = false;
			if( thread == null || !thread.isAlive() )
			{	thread = new Thread(this);
				thread.start();
			}
		}
		public void stop()
		{	stopSignalled = true;
		}
			
		public void run()
		{	/**/ Defs.debug( "PrepThread.run starts" );
			while( !stopSignalled  )
			{	if( (elementInPrep + 1) % dataLength != elementInUse )
				{	// there is still one to work on
					elementInPrep = (elementInPrep + 1) % dataLength;				// advance working image
					/**/ Defs.debug( "PrepThread disp="+elementInUse+" prep="+elementInPrep+" frame="+prepNum );
					parent.prepareFrame( images[elementInPrep] );		// prepare the frame
					prepNum++;
				}
				else
				{	/**/ Defs.debug( "PrepThread snoozing" );
					Defs.snooze( prepDelay );
				}
			}
			/**/ Defs.debug( "PrepThread.run ends" );
		}
	}
}